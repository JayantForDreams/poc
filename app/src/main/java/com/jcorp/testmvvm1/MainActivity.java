package com.jcorp.testmvvm1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    //1. Satin App starts the psychometric app.
    /**
     * From satin app below code is used to start psychometric app
     * @param view
     */
    // Uri myAction = Uri.parse("https://satincreditcare.app/test/" + apiToken + "/" + testType
    //      + "/" + referenceNumber + "/" + centerId + "/" + userId);
    // PackageManager packageManager = getActivity().getPackageManager();
    // Intent intent = packageManager.getLaunchIntentForPackage("com.satincreditcare.psychometric");
    //     if (intent != null) {
    //            intent.setAction(Intent.ACTION_VIEW);
    //            intent.setData(myAction);
    //            startActivity(intent);
    //     }

    //2. To Receive/Capture data in Psychometric app.
    /**
     * To see How intent data is captured see in AndroidManifest.xml
     */


    //3. To Send  Data Back to satin app.
    /**
     * Warp data in Intent Object & Just had to call setResult() and pass Intent object in parameters .
     * @param view
     */
    public void clickBtn(View view){
        Intent intent = new Intent();
        intent.putExtra("PSYCHO_RESULT", "SELECTED");
        setResult(RESULT_OK, intent);//Need to implement similar to this method in IONIC
        finish();
    }
}
